const GENESYS_DATE = new Date(2009, 0, 3, 18, 15, 0);

const today = Date.now();

const diff = Math.abs(today - GENESYS_DATE);

// console.log(diff / 1000 / 60 / 525600 * 4, 2);
const sizeYears = Math.floor(diff / 1000 / 60 / 525600) * 4.2;
// console.log("Tailles des entetes : " + sizeYears + " Mb");
const sizeMinutes10 = Math.floor(diff / 1000 / 60 % 525600) / 10 * 80 / 1000000;
// console.log("Tailles des entetes restantes : " + sizeMinutes10 + " Mb");
console.log("Tailles totales des entêtes : " + (sizeYears + sizeMinutes10) + " Mb");
